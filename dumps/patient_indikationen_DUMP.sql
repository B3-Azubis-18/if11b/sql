-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 20. Apr 2015 um 08:53
-- Zuletzt verändert: 27. Jan 2020 um 21:19
-- Server Version: 5.5.32
-- PHP-Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `krankenhaus`
--
CREATE DATABASE IF NOT EXISTS `krankenhaus` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `krankenhaus`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `inventar`
--

CREATE TABLE IF NOT EXISTS `inventar` (
  `Nr` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) DEFAULT NULL,
  `Art` varchar(20) DEFAULT NULL,
  `Nr_Patient` int(11) DEFAULT '0',
  PRIMARY KEY (`Nr`),
  KEY `Nr_Patient` (`Nr_Patient`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Daten für Tabelle `inventar`
--

INSERT INTO `inventar` (`Nr`, `Name`, `Art`, `Nr_Patient`) VALUES
(1, 'Bett', 'Möbel', 24),
(2, 'Telefon', 'Kommunikation', 24),
(3, 'Bett', 'Möbel', 2506),
(4, 'Beatmungsgerät', 'Medizin', 2506),
(5, 'Bett', 'Möbel', 2654),
(6, 'Bett', 'Möbel', 319),
(7, 'Schrank', 'Möbel', 24);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `indikationen`
--

CREATE TABLE IF NOT EXISTS `indikationen` (
  `Nr` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Erbkrankheit` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Nr`),
  KEY `Name` (`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Daten für Tabelle `indikationen`
--

INSERT INTO `indikationen` (`Nr`, `Name`, `Erbkrankheit`) VALUES
(1, 'Größenwahn', 0),
(2, 'Akne', 0),
(3, 'Übergewicht', 0),
(4, 'Bluthochdruck', 0),
(5, 'Hausstauballergie', 0),
(6, 'Rheuma', 0),
(7, 'Gicht', 0),
(8, 'Bulimie', 0),
(9, 'Eisenmangel', 0),
(10, 'Fieber', 0),
(11, 'Glaukom', 0),
(12, 'Hepatitis', 0),
(13, 'Keuchhusten', 0),
(14, 'Migräne', 0),
(15, 'Meningitis', 0),
(16, 'Orangenhaut', 0),
(17, 'Sodbrennen', 0),
(18, 'Höhenangst', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `patienten`
--

CREATE TABLE IF NOT EXISTS `patienten` (
  `Nr` int(11) NOT NULL,
  `Nachname` varchar(40) DEFAULT NULL,
  `Vorname` varchar(40) DEFAULT NULL,
  `Anrede` varchar(10) DEFAULT NULL,
  `Titel` varchar(15) DEFAULT NULL,
  `Straße` varchar(40) DEFAULT NULL,
  `PLZ` varchar(6) DEFAULT NULL,
  `Ort` varchar(40) DEFAULT NULL,
  `Telefon` varchar(20) DEFAULT NULL,
  `Geburtstag` datetime DEFAULT NULL,
  `Alter` int(11) DEFAULT '0',
  `Geschlecht` varchar(1) DEFAULT NULL,
  `Bemerkung` text,
  `Aufnahmedatum` datetime DEFAULT NULL,
  `Lastcall` datetime DEFAULT NULL,
  `Krankenkasse` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `patienten`
--

INSERT INTO `patienten` (`Nr`, `Nachname`, `Vorname`, `Anrede`, `Titel`, `Straße`, `PLZ`, `Ort`, `Telefon`, `Geburtstag`, `Alter`, `Geschlecht`, `Bemerkung`, `Aufnahmedatum`, `Lastcall`, `Krankenkasse`) VALUES
(1, 'Aufrecht', 'Georg', 'Herr', NULL, 'Gibitzenhofstraße 34', '8500', 'Nürnberg', '0911 / 452311', '1941-01-12 00:00:00', 61, 'M', 'erhält gelbe Pillen -> Multi-Aethanol Konzentrat;\r\nlegt Wert auf eine ausführliche Beratung\r\n', '2013-06-28 00:00:00', '2017-10-14 00:00:00', 'DBVK'),
(2, 'Augat', 'Franz', 'Herr', NULL, 'Gaismannshofstraße 27a', '8500', 'Nürnberg', NULL, '1912-12-12 00:00:00', 90, 'M', NULL, '2012-06-30 00:00:00', '2014-11-22 00:00:00', 'DBVK'),
(3, 'Augsburger', 'Hermann', 'Herr', NULL, 'Neuer Weg 10', '6600', NULL, NULL, '1954-11-12 00:00:00', 48, 'M', NULL, '2012-05-12 00:00:00', '2012-08-03 00:00:00', 'BEK'),
(5, 'Augustin', 'Barbara', 'Frau', NULL, 'Wilhemsdorfer Straße 32', '8503', 'Hinterbaldrian', NULL, '1949-11-17 00:00:00', 53, 'W', NULL, '2012-03-13 00:00:00', '2012-03-13 00:00:00', 'AOK'),
(6, 'Augustin', 'Dieter', 'Herr', NULL, NULL, NULL, NULL, NULL, '1952-07-31 00:00:00', 50, 'M', NULL, '2013-04-29 00:00:00', '2013-04-29 00:00:00', 'AOK'),
(7, 'Augustin', 'Hans', 'Herr', NULL, NULL, '8500', 'Nürnberg', NULL, '1962-02-12 00:00:00', 40, 'M', NULL, '2012-07-02 00:00:00', '2012-07-02 00:00:00', NULL),
(8, 'Augustus', 'Oktavianus', 'Caesar', 'Imperator', 'Forum Romanum 1', NULL, 'Rom', NULL, '1972-05-04 00:00:00', 30, 'M', NULL, '2012-05-08 00:00:00', '2012-05-08 00:00:00', 'BEK'),
(9, 'Auler', 'Hans', 'Herr', NULL, 'Ludwigstraße 34a', NULL, NULL, NULL, '1941-08-25 00:00:00', 61, 'M', NULL, '2012-07-07 00:00:00', '2012-07-07 00:00:00', 'AOK'),
(10, 'Baader', 'Berta', 'Frau', NULL, NULL, '8500', 'Fürth', NULL, '1986-07-12 00:00:00', 16, 'W', NULL, '2013-06-23 00:00:00', '2013-06-23 00:00:00', 'AOK'),
(11, 'Raader', 'Heinrich', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 65, 'M', NULL, '2011-08-20 00:00:00', '2011-08-20 00:00:00', NULL),
(12, 'Babel', 'Adolf', NULL, NULL, NULL, NULL, NULL, NULL, '1952-07-01 00:00:00', 50, 'M', NULL, '2012-08-20 00:00:00', '2012-08-20 00:00:00', NULL),
(13, 'Babel', 'Ilse', NULL, NULL, NULL, NULL, NULL, NULL, '1931-12-12 00:00:00', 71, 'W', NULL, '2012-04-23 00:00:00', '2012-04-23 00:00:00', 'AOK'),
(14, 'Bablischky', 'Inge', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50, 'W', NULL, '2012-05-07 00:00:00', '2012-05-07 00:00:00', NULL),
(15, 'Bachmann', 'Christiane', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 'W', NULL, '2012-05-07 00:00:00', '2012-05-07 00:00:00', 'AOK'),
(16, 'Bachmann', 'Josef', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 'M', NULL, '2011-03-15 00:00:00', '2011-03-15 00:00:00', NULL),
(17, 'Bachmann', 'Klaus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, 'M', NULL, '2011-02-14 00:00:00', '2011-02-14 00:00:00', 'BKK'),
(18, 'Bachmann', 'Ursula', 'Frau', NULL, NULL, NULL, NULL, NULL, '1963-08-13 00:00:00', 39, 'W', NULL, '2012-08-10 00:00:00', '2012-08-10 00:00:00', 'BKK'),
(19, 'Bachmayer', 'Anne', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55, 'W', NULL, '2011-12-12 00:00:00', '2011-12-12 00:00:00', 'BKK'),
(20, 'Bachmeier', 'Alexander', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 'M', NULL, '2011-11-11 00:00:00', '2011-11-11 00:00:00', 'BKK'),
(21, 'Bachmeier', 'Maria', 'Frau', NULL, 'Am Burgring 34', '78004', 'Meilingen', NULL, '1951-11-01 00:00:00', 51, 'W', NULL, '2012-07-08 00:00:00', '2012-07-08 00:00:00', NULL),
(22, 'Bachstein', 'Rolf', 'Herr', NULL, NULL, NULL, NULL, NULL, '1948-04-21 00:00:00', 54, 'M', NULL, '2012-04-24 00:00:00', '2012-06-24 00:00:00', NULL),
(23, 'Backhausen', 'Monika', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 65, 'W', NULL, '2011-08-08 00:00:00', '2011-08-08 00:00:00', 'BKK'),
(24, 'Beckenbauer', 'Franz', NULL, NULL, 'Kaiserallee 2', '90456', 'München', NULL, '1952-05-10 00:00:00', 50, 'M', NULL, '2011-07-07 00:00:00', '2011-07-07 00:00:00', NULL),
(25, 'Bäcker', 'Kerstin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55, 'W', NULL, '2011-06-06 00:00:00', '2011-06-06 00:00:00', NULL),
(26, 'Beckstein', 'Günther', 'Herr', NULL, NULL, '8500', 'Nürnberg', NULL, '1938-01-31 00:00:00', 64, 'M', NULL, '2011-05-05 00:00:00', '2011-05-05 00:00:00', NULL),
(27, 'Brandt', 'Willi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 78, 'M', NULL, '2012-08-03 00:00:00', '2012-08-03 00:00:00', NULL),
(28, 'Bubka', 'Sergej', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23, 'M', NULL, '2012-08-10 00:00:00', '2012-08-10 00:00:00', 'DBVK'),
(29, 'Bittner', 'Clemens', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, 'M', 'Das ist ein Testbemerkung für Herrn Clemens Bittner. Dieser Kunde \r\nwünscht sich immer eine ...', '2011-02-02 00:00:00', '2011-02-02 00:00:00', NULL),
(30, 'Cooper', 'Gary', 'Mister', NULL, NULL, NULL, 'Hollywood', NULL, '1962-08-17 00:00:00', 40, 'M', NULL, '2011-01-31 00:00:00', '2011-01-31 00:00:00', 'BKK'),
(31, 'Bunte', 'Georg', 'Herr', NULL, NULL, NULL, NULL, NULL, '1931-05-28 00:00:00', 71, 'M', NULL, '2013-06-28 00:00:00', '2013-06-28 00:00:00', 'BKK'),
(32, 'Bunte', 'Gunda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 56, 'W', NULL, '2012-06-30 00:00:00', '2012-08-03 00:00:00', 'AOK'),
(33, 'Brettner', 'Johannes', 'Herr', NULL, 'Böllerstraße 13', '8500', 'Nürnberg', NULL, '1911-06-17 00:00:00', 91, 'M', NULL, '2012-03-16 00:00:00', '2012-03-16 00:00:00', 'AOK'),
(34, 'Bittner', 'Markus', 'Herr', NULL, 'Lehrer Straße 4', '8500', 'Nürnberg', NULL, '1967-03-12 00:00:00', 35, 'M', 'hgasfjkd adfjgaskf asfhdgask asfhdkjsfhdsa fasfhdkjasfd sdfjhasjdf ì\nsadfhsdf safjdhasfkdhasdf sdfhaskfhds jksd\r\nsdfkajksfhd sjkfdhlasfd sfadhsfldhajlsfhda fjkasfhdlfhdajs \r\nsjkfhsfkdhskfd sdj sdfjhksfhskf', '2011-09-27 00:00:00', '2011-09-27 00:00:00', 'AOK'),
(35, 'Caesar', 'Gaius Julius', NULL, 'Imperator', 'Forum Romanum 1', NULL, 'Rom', NULL, '1939-08-05 00:00:00', 63, 'M', NULL, '2012-03-13 00:00:00', '2012-03-13 00:00:00', NULL),
(36, 'Carini', 'Christiane', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23, 'W', NULL, '2011-07-25 00:00:00', '2011-07-25 00:00:00', NULL),
(38, 'Carli', 'Michele', 'Miss', NULL, 'Avenue de Charly Brown', '65', 'Noucreton', '0077 552 2323', '1963-05-11 00:00:00', 39, 'W', NULL, '2011-05-23 00:00:00', '2011-05-23 00:00:00', NULL),
(39, 'Castro', 'Fidel', 'Herr', NULL, 'Revolutionsweg 1', NULL, 'Havanna', '00213 778 21313', '1915-07-05 00:00:00', 87, 'M', NULL, '2013-06-28 00:00:00', '2015-03-06 00:00:00', 'AOK'),
(40, 'Dusel', 'Elisabeth', 'Frau', NULL, 'Boelckestraße 13', '8500', 'Nürnberg', NULL, '1965-06-23 00:00:00', 37, 'W', NULL, '2011-03-21 00:00:00', '2011-03-21 00:00:00', NULL),
(41, 'Dusel', 'Mausi', 'Frau', NULL, 'Hufestraße 36', '8500', 'Nürnberg', NULL, '1943-11-23 00:00:00', 59, 'W', NULL, '2011-02-20 00:00:00', '2011-02-20 00:00:00', 'AOK'),
(42, 'Dürer', 'Albrecht', 'Herr', NULL, 'Dürerplatz 1', '90032', 'Nürnberg', NULL, NULL, 56, 'M', NULL, '2012-08-03 00:00:00', '2015-03-08 00:00:00', NULL),
(43, 'Eckstein', 'Dieter', NULL, NULL, NULL, NULL, 'Frankfurt', '0454 / 34 00 92', '1912-12-12 00:00:00', 90, 'M', NULL, '2011-12-18 00:00:00', '2011-12-18 00:00:00', NULL),
(44, 'Einstein', 'Albert', NULL, NULL, NULL, NULL, NULL, NULL, '2011-12-12 00:00:00', 11, 'M', NULL, '2011-11-17 00:00:00', '2011-11-17 00:00:00', NULL),
(45, 'Fischer', 'Johannes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 'M', NULL, '2011-10-16 00:00:00', '2011-10-16 00:00:00', NULL),
(46, 'Fitz', 'Lisa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 66, 'W', NULL, '2011-10-15 00:00:00', '2011-10-15 00:00:00', NULL),
(47, 'Friedrich', 'Marion', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 'W', NULL, '2011-09-14 00:00:00', '2011-09-14 00:00:00', NULL),
(48, 'Graf', 'Steffi', 'Frl.', NULL, NULL, NULL, 'Brühl', NULL, NULL, 70, 'W', NULL, '2012-05-07 00:00:00', '2012-05-07 00:00:00', 'BEK'),
(49, 'Graf', 'Waltraud', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 90, 'W', NULL, '2011-07-12 00:00:00', '2011-07-12 00:00:00', 'BEK'),
(50, 'Grüner', 'Roland', NULL, NULL, NULL, '90234', 'Fürth', NULL, '1958-07-23 00:00:00', 44, 'M', NULL, '2013-04-29 00:00:00', '2013-04-29 00:00:00', 'BEK'),
(51, 'Haack', 'Marion', NULL, NULL, NULL, NULL, NULL, NULL, '1934-06-09 00:00:00', 68, 'W', NULL, '2011-05-10 00:00:00', '2015-03-08 00:00:00', 'BEK'),
(52, 'Henkel', 'Heike', NULL, NULL, NULL, NULL, NULL, NULL, '1963-05-23 00:00:00', 39, 'W', NULL, '2011-04-09 00:00:00', '2011-04-09 00:00:00', NULL),
(53, 'Hoffmann', 'Willi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 78, 'M', NULL, '2011-03-08 00:00:00', '2011-03-08 00:00:00', 'AOK'),
(54, 'Höneß', 'Uli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22, 'M', NULL, '2011-02-07 00:00:00', '2011-02-07 00:00:00', 'AOK'),
(55, 'Köpcke', 'Andreas', 'Herr', NULL, NULL, NULL, 'Beyersdorf', NULL, '1944-11-02 00:00:00', 58, 'M', NULL, '2011-01-06 00:00:00', '2011-01-06 00:00:00', 'AOK'),
(56, 'Krabbe', 'Katrin', 'Frau', NULL, NULL, NULL, NULL, NULL, '1963-05-14 00:00:00', 39, 'W', NULL, '2011-05-31 00:00:00', '2011-05-31 00:00:00', 'AOK'),
(57, 'Kraus', 'Stefanie', NULL, NULL, NULL, NULL, NULL, NULL, '1964-09-12 00:00:00', 38, 'W', NULL, '2011-04-30 00:00:00', '2011-04-30 00:00:00', 'AOK'),
(58, 'Krausela', 'Birgit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 'W', NULL, '2011-03-29 00:00:00', '2011-03-29 00:00:00', 'AOK'),
(59, 'Krausela', 'Ingrid', 'Frau', NULL, NULL, NULL, NULL, NULL, '1981-03-08 00:00:00', 21, 'W', NULL, '2011-02-28 00:00:00', '2015-02-20 00:00:00', 'DBVK'),
(60, 'Mann', 'Golo', 'Herr', NULL, NULL, NULL, NULL, NULL, NULL, 40, 'M', NULL, '2011-01-27 00:00:00', '2015-03-09 00:00:00', 'DBVK'),
(61, 'Maier', 'Sepp', 'Herr', 'Torwart-Trainer', 'Bayernstarße 4', '80033', 'München-Anzing', NULL, NULL, 30, 'M', NULL, '2012-07-17 00:00:00', '2015-03-09 00:00:00', 'DBVK'),
(62, 'Meier', 'Silke', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 'W', NULL, '2011-11-25 00:00:00', '2011-11-25 00:00:00', 'AOK'),
(63, 'Meyer', 'Erwin', 'Herr', NULL, 'Würzburger Straße 1', NULL, 'Heidelbeerweiler', '09132 / 434 11', NULL, 50, 'M', NULL, '2011-10-24 00:00:00', '2011-10-24 00:00:00', 'DBVK'),
(64, 'Meyfarth', 'Ulrike', NULL, NULL, NULL, NULL, NULL, NULL, '1962-03-21 00:00:00', 40, 'W', NULL, '2010-09-25 00:00:00', '2010-09-25 00:00:00', 'AOK'),
(65, 'Meysel', 'Inge', NULL, NULL, NULL, NULL, NULL, NULL, '1955-06-22 00:00:00', 47, 'W', NULL, '2010-08-26 00:00:00', '2010-08-26 00:00:00', 'AOK'),
(66, 'Muthlos', 'Andrea', 'Frau', NULL, 'Leyher Straße 45', '8500', NULL, NULL, NULL, 77, 'W', NULL, '2010-06-27 00:00:00', '2010-06-27 00:00:00', 'AOK'),
(67, 'Muth', 'Hermeline', 'Frau', NULL, 'Kieler Weg 10', '8401', NULL, '09132 / 1234', NULL, 34, 'W', NULL, '2010-05-28 00:00:00', '2010-05-28 00:00:00', NULL),
(68, 'Müller', 'Franz', NULL, NULL, 'Julius Lossmann Straße 12', '8500', 'Nürnberg', NULL, NULL, 34, 'M', NULL, '2010-11-01 00:00:00', '2010-11-01 00:00:00', NULL),
(69, 'Müller', 'Gerd', NULL, NULL, 'Schwabinger Ring 221', '8000', 'München 2', NULL, NULL, 65, 'M', NULL, '2010-10-02 00:00:00', '2010-10-02 00:00:00', NULL),
(70, 'Prey', 'Herrmann', 'Herr', NULL, NULL, NULL, NULL, NULL, '2011-12-02 00:00:00', 11, 'M', NULL, '2010-09-03 00:00:00', '2010-09-03 00:00:00', NULL),
(71, 'Richter', 'Annegret', NULL, NULL, NULL, NULL, NULL, NULL, '1932-09-16 00:00:00', 70, 'W', NULL, '2010-08-04 00:00:00', '2010-08-04 00:00:00', 'BKK'),
(72, 'Rothenberger', 'Anneliese', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 'W', NULL, '2010-07-05 00:00:00', '2010-07-05 00:00:00', 'BKK'),
(73, 'Rush', 'Jennifer', NULL, NULL, NULL, '90000', 'München', NULL, '1982-12-13 00:00:00', 20, 'W', NULL, '2010-06-06 00:00:00', '2010-06-06 00:00:00', 'BKK'),
(74, 'Schmidt', 'Andreas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 'M', NULL, '2010-05-07 00:00:00', '2010-05-07 00:00:00', 'TKK'),
(75, 'Schmidt', 'Walter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 'M', NULL, '2010-04-08 00:00:00', '2010-04-08 00:00:00', 'TKK'),
(76, 'Schmitt', 'Andreas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 88, 'M', NULL, '2010-03-09 00:00:00', '2010-03-09 00:00:00', 'TKK'),
(77, 'Schmitt', 'Gerhart', NULL, NULL, NULL, NULL, NULL, NULL, '1957-02-13 00:00:00', 45, 'M', NULL, '2012-04-23 00:00:00', '2012-04-23 00:00:00', 'BEK'),
(78, 'Seeler', 'Uwe', 'Herr', NULL, 'Idolweg 5', '90566', 'Nürnberg', NULL, NULL, 67, 'M', NULL, '2012-02-27 00:00:00', '2015-03-09 00:00:00', 'BEK'),
(79, 'Strauß', 'Franz Josef', 'Herr', 'Dr.', 'Wolke 23', '00345', 'Himmelblau', NULL, NULL, 56, 'M', NULL, '2011-12-12 00:00:00', '2011-12-12 00:00:00', 'AOK'),
(80, 'Streibel', 'Max', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 'M', NULL, '2011-11-13 00:00:00', '2011-11-13 00:00:00', NULL),
(81, 'Unsinn', 'Xaver', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 75, 'M', NULL, '2011-10-14 00:00:00', '2011-10-14 00:00:00', NULL),
(82, 'Übelohr', 'Susi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 'W', NULL, '2011-09-15 00:00:00', '2011-09-15 00:00:00', 'AOK'),
(83, 'Walitza', 'Hans', 'Herr', NULL, NULL, NULL, NULL, NULL, NULL, 34, 'M', NULL, '2011-08-16 00:00:00', '2011-08-16 00:00:00', 'AOK'),
(84, 'Wehner', 'Herbert', 'Herr', NULL, NULL, NULL, 'Bonn', NULL, NULL, 20, 'M', NULL, '2011-07-17 00:00:00', '2011-07-17 00:00:00', 'AOK'),
(85, 'von Weizsäcker', 'Carl Friedrich', 'Herr', NULL, NULL, NULL, NULL, NULL, NULL, 67, 'M', NULL, '2011-06-18 00:00:00', '2015-03-09 00:00:00', 'TKK'),
(86, 'Weiß', 'Gerhard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 'M', NULL, '2012-08-03 00:00:00', '2012-08-03 00:00:00', 'AOK'),
(87, 'Weiß', 'Heidelinde', NULL, NULL, 'Frau in Weiß Platz 1', NULL, NULL, NULL, '1953-04-12 00:00:00', 49, 'W', NULL, '2011-01-23 00:00:00', '2015-03-06 00:00:00', 'TKK'),
(88, 'Wiese', 'Maria', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 70, 'W', NULL, '2011-02-22 00:00:00', '2011-02-22 00:00:00', 'NOK'),
(89, 'Xaver', 'Franz', 'Herr', NULL, NULL, NULL, NULL, NULL, NULL, 35, 'M', NULL, '2011-03-21 00:00:00', '2011-03-21 00:00:00', NULL),
(90, 'Xaver', 'Joe', 'Herr', NULL, NULL, NULL, NULL, NULL, NULL, 50, 'M', NULL, '2011-04-20 00:00:00', '2011-04-20 00:00:00', NULL),
(91, 'von Kleinschwarzenlohe am Niederrhein', 'Heinrich Friedhelm', 'Herr', 'Baron', NULL, NULL, NULL, NULL, NULL, 56, 'M', NULL, '2010-01-31 00:00:00', '2010-01-31 00:00:00', NULL),
(92, 'vom Berg', 'Ludwig', 'Herr', 'Graf', 'Burgenstr. 123', '2356', 'Schloßberg', '24355  566  55656', '1912-12-12 00:00:00', 90, 'M', NULL, '2014-01-01 00:00:00', '2014-06-10 00:00:00', 'DFB'),
(93, 'Stanjek', 'Eberhard', NULL, NULL, NULL, NULL, 'München', NULL, NULL, 38, 'M', NULL, '2014-01-01 00:00:00', '2014-11-25 00:00:00', 'DFB'),
(94, 'Preißdörfer', 'Marc', 'Geißi', 'Duke', 'Pferdtaler Str. 12', '8500', 'Nürnberg', '0911 / 32 96 96', '1976-03-20 00:00:00', 26, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', 'NOK'),
(95, 'Hase', 'Hans', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(96, 'Kraxlhuber', 'Erwin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(97, 'Kraxlmeier', 'Edi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 60, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(98, 'Barbecue', 'Claudia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 'W', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', 'TKK'),
(99, 'Rußknecht', 'Peter', 'Herr', 'Professor', 'Roßtaler Str. 16a', '8500', 'Nürnberg', '0911 / 31 55 59', '1975-08-11 00:00:00', 27, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', 'NOK'),
(101, 'Müller', 'Heinrich', 'Herr', 'Dr.', NULL, '8500', 'Nürnberg', '0911 / 40 78 01', '1962-06-14 00:00:00', 40, 'M', NULL, '2010-02-28 00:00:00', '2010-02-28 00:00:00', 'TKK'),
(102, 'Mayer', 'Klaus', 'Herr', NULL, NULL, NULL, NULL, NULL, NULL, 40, 'M', NULL, '2010-03-30 00:00:00', '2010-03-30 00:00:00', 'TKK'),
(103, 'Pater', 'Joe', 'Herr', NULL, 'Great Western Street 23', NULL, NULL, NULL, NULL, 34, 'M', NULL, '2010-04-29 00:00:00', '2010-04-29 00:00:00', NULL),
(104, 'Pabl', 'Friedhelm', 'Herr', 'Dr.', 'Züricher Hauptstraße 23', '7878', 'Neualbing', NULL, '1921-07-31 00:00:00', 81, 'M', NULL, '2011-01-14 00:00:00', '2011-01-14 00:00:00', NULL),
(119, 'Holzer', 'Klaus', 'Herr', NULL, NULL, NULL, NULL, NULL, '1963-06-12 00:00:00', 39, 'M', NULL, '2012-01-13 00:00:00', '2012-01-13 00:00:00', 'BKK'),
(120, 'Mayr', 'Gisela', 'Frau', NULL, NULL, NULL, NULL, NULL, '1967-05-03 00:00:00', 35, 'W', NULL, '2011-05-14 00:00:00', '2011-05-14 00:00:00', 'AOK'),
(121, 'Künkel', 'Friedhelm', 'Herr', 'Dr.', NULL, NULL, NULL, NULL, '1948-05-23 00:00:00', 54, 'M', NULL, '2010-06-27 00:00:00', '2010-06-27 00:00:00', 'TKK'),
(122, 'Masern', 'Hans', 'Herr', NULL, NULL, '4333', 'Oberklinik', NULL, '1921-09-12 00:00:00', 81, 'M', NULL, '2011-08-03 00:00:00', '2015-03-06 00:00:00', NULL),
(127, 'Klauser', 'Ingeborg', 'Frau', NULL, NULL, NULL, NULL, NULL, '1971-01-23 00:00:00', 31, 'W', NULL, '2012-02-27 00:00:00', '2012-02-27 00:00:00', NULL),
(128, 'Schmid', 'Karl', 'Master', NULL, 'Burgstraße 154', '8500', 'Nürnberg', '0911 / 311716', '1967-08-22 00:00:00', 35, 'M', NULL, '2012-03-16 00:00:00', '2012-03-16 00:00:00', NULL),
(129, 'Mayerling', 'Herrmann', 'Herr', NULL, NULL, NULL, NULL, NULL, '1952-08-12 00:00:00', 50, 'M', NULL, '2012-04-23 00:00:00', '2012-04-23 00:00:00', 'AOK'),
(130, 'Mustermann', 'Paul', 'Herr', NULL, 'Schwarzwälder Straße 18', '8500', 'Nürnberg', '0999/9090', '1948-01-12 00:00:00', 54, 'M', NULL, '2012-04-28 00:00:00', '2000-04-05 00:00:00', 'AOK'),
(190, 'Hinterhuber', 'Isolde', 'Frau', 'Dr.', NULL, NULL, NULL, NULL, '1918-04-14 00:00:00', 84, 'W', NULL, '2012-07-10 00:00:00', '2012-07-10 00:00:00', 'AOK'),
(192, 'Karlsbad', 'Claudia', 'Frau', 'Prof.', NULL, NULL, NULL, NULL, '1963-12-09 00:00:00', 39, 'W', NULL, '2012-07-17 00:00:00', '2012-07-17 00:00:00', NULL),
(204, 'Hübscher', 'Josef', 'Herr', NULL, NULL, NULL, NULL, NULL, '1952-07-12 00:00:00', 50, 'M', NULL, '2012-08-11 00:00:00', '2012-08-11 00:00:00', NULL),
(205, 'Haselhuber', 'Ingrid', 'Frau', 'Dr.', NULL, '8000', 'München 1', NULL, '1931-01-27 00:00:00', 71, 'W', NULL, '2012-08-11 00:00:00', '2013-06-28 00:00:00', NULL),
(300, 'Headcrash', 'Oweia', 'Frau', NULL, 'Straße der Glückseligkeiten 81', '90052', 'Nürnberg', '0911 / 10 67 15', '1939-10-10 00:00:00', 63, 'W', NULL, NULL, '2015-04-26 00:00:00', NULL),
(301, 'Lessing', 'Gotthold Ephraim', 'Herr', NULL, 'Dichterstraße 62', '90031', 'Nürnberg', '0911 / 11 51 66', '1910-06-17 00:00:00', 92, 'M', NULL, NULL, '2015-03-09 00:00:00', NULL),
(302, 'Amselfelder', 'Ziseline', 'Frau', NULL, 'Brauereistraße 48', '90016', 'Nürnberg', '0911 / 12 25 92', '2012-05-09 00:00:00', 10, 'W', NULL, NULL, '2015-03-06 00:00:00', NULL),
(303, 'Ziegenpeter', 'Yolande', 'Frau', NULL, 'Krankenweg 21', '90140', 'Fürth', '0911 / 11 43 15', '1918-10-28 00:00:00', 84, 'W', NULL, NULL, '2015-04-26 00:00:00', NULL),
(304, 'Illgner', 'Bodo', 'Herr', NULL, 'Torwartstraße 89', '90002', 'Nürnberg', '0911 / 13 29 25', '1901-11-25 00:00:00', 101, 'M', NULL, NULL, '2015-03-09 00:00:00', NULL),
(305, 'Fittipaldi', 'Emmerson', 'Herr', NULL, 'Ringstraße 60', '90121', 'Fürth', '0911 / 16 34 54', '1921-05-31 00:00:00', 81, 'M', NULL, NULL, '2015-03-06 00:00:00', NULL),
(306, 'Barnard', 'Christian', 'Herr', NULL, 'Herzstraße 40', '90015', 'Nürnberg', '0911 / 13 10 69', '1940-03-06 00:00:00', 62, 'M', NULL, NULL, '2015-03-08 00:00:00', NULL),
(307, 'Klose', 'Maria', 'Fräulein', NULL, NULL, NULL, NULL, NULL, '1942-07-25 00:00:00', 60, 'W', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(308, 'Riedelknecht', 'Heinz', 'Herr', NULL, NULL, NULL, NULL, NULL, '1973-11-01 00:00:00', 29, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(309, 'Höllerich', 'Irmgard', 'Frau', NULL, NULL, NULL, NULL, NULL, '1978-09-09 00:00:00', 24, 'W', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(310, 'Hisel', 'Herbert', 'Herr', NULL, 'Witzboldweg 5', '90085', 'Nürnberg', '0911 / 11 23 68', '1987-08-23 00:00:00', 15, 'M', 'Nürnberger Origial', NULL, '2015-03-09 00:00:00', 'BKK'),
(311, 'Wilder', 'Gene', 'Herr', NULL, 'Hollywood Weg 2', '90002', 'Nürnberg', '0911 / 14 19 53', '2012-06-15 00:00:00', 10, 'M', NULL, NULL, '2015-03-08 00:00:00', NULL),
(312, 'Casanova', 'Giacomo Girolamo', 'Herr', NULL, NULL, NULL, NULL, NULL, '1965-02-11 00:00:00', 37, 'M', NULL, '2014-01-01 00:00:00', '2015-03-08 00:00:00', 'BKK'),
(313, 'Meir', 'Golda', 'Frau', NULL, NULL, NULL, NULL, NULL, '1961-03-12 00:00:00', 41, 'W', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(314, 'Clinton', 'Bill', 'Herr', 'Dr.', 'Präsidenten Allee 45', '49900', 'Washington', NULL, '1911-10-05 00:00:00', 91, 'M', NULL, '2014-01-01 00:00:00', '2015-04-26 00:00:00', 'PRIV'),
(315, 'Reagan', 'Ronald', 'Herr', NULL, NULL, NULL, NULL, NULL, '1952-02-06 00:00:00', 50, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(316, 'Hirsch', 'Harry', 'Herr', NULL, 'Auf der Gasse 16', '90132', 'Fürth', '0911 / 15 77 15', '1961-04-14 00:00:00', 41, 'M', NULL, NULL, '2015-04-26 00:00:00', NULL),
(317, 'Baiernhammer', 'Gustl', 'Herr', NULL, 'Theater Weg 23', '90086', 'Nürnberg', '0911 / 10 67 90', '1911-06-13 00:00:00', 91, 'M', NULL, NULL, '2015-03-06 00:00:00', NULL),
(318, 'von Suttner', 'Berta', 'Frau', NULL, NULL, NULL, NULL, NULL, '1981-08-12 00:00:00', 21, 'W', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(319, 'Malden', 'Karl', 'Herr', NULL, 'San Fransisco Street 61', '90188', 'Fürth', '0911 / 10 08 51', '2014-06-24 00:00:00', 8, 'M', NULL, NULL, '2015-08-03 00:00:00', NULL),
(2141, 'Freud', 'Sigmund', 'Herr', NULL, 'Philosophie Straße 41', '90078', 'Nürnberg', '0911 / 15 25 88', '1975-10-26 00:00:00', 27, 'M', NULL, NULL, '2015-03-06 00:00:00', 'AOK'),
(2316, 'Spock', NULL, 'Mister', NULL, 'Deck 49', '70041', 'Enterprise', '0911 / 15 06 48', '1946-09-25 00:00:00', 56, 'M', NULL, NULL, '2015-03-06 00:00:00', 'BKK'),
(2360, 'Hitchcock', 'Alfred', 'Herr', NULL, 'Grusel Platz 44', '90053', 'Nürnberg', '0911 / 16 52 72', '1923-04-02 00:00:00', 79, 'M', NULL, NULL, '2015-03-06 00:00:00', NULL),
(2506, 'Lemmon', 'Jack', 'Herr', NULL, 'Heißweg 11', '90466', 'Zirndorf', '0911 / 10 71 28', '1913-07-13 00:00:00', 89, 'M', NULL, NULL, '2015-03-09 00:00:00', NULL),
(2588, 'Crawford', 'Cindy', 'Frau', NULL, 'Hollywood Straße 86', '90008', 'Nürnberg', '0911 / 10 04 74', '1910-03-11 00:00:00', 92, 'W', NULL, NULL, '2015-03-06 00:00:00', NULL),
(2654, 'Bronson', 'Charles', 'Herr', NULL, 'Roter Mann Straße 65', '90115', 'Fürth', '0911 / 11 53 27', '1927-12-06 00:00:00', 75, 'M', NULL, NULL, '2015-03-08 00:00:00', NULL),
(2686, 'Spencer', 'Bud', 'Herr', NULL, NULL, NULL, 'Hollywood', NULL, '1949-11-20 00:00:00', 53, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(2705, 'Schiffer', 'Claudia', 'Fräulein', NULL, 'Park Avenue 4711', NULL, 'Hollywood', NULL, '1911-05-12 00:00:00', 91, 'W', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', NULL),
(2753, 'Kirk', 'Jim', 'Mister', 'Captain', 'Deck 22', 'U.S.S.', 'Enterprise', NULL, '1949-07-20 00:00:00', 53, 'M', NULL, '2014-01-01 00:00:00', '2014-01-01 00:00:00', 'BKK');
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `patienten_indikationen`
--

CREATE TABLE IF NOT EXISTS `patienten_indikationen` (
  `Nr_Patient` int(11) NOT NULL DEFAULT '0',
  `Nr_Indikation` int(11) NOT NULL DEFAULT '0',
  `ZugeordnetAm` date DEFAULT NULL,
  PRIMARY KEY (`Nr_Patient`,`Nr_Indikation`),
  KEY `Nr_Indikation` (`Nr_Indikation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `patienten_indikationen`
--

INSERT INTO `patienten_indikationen` (`Nr_Patient`, `Nr_Indikation`, `ZugeordnetAm`) VALUES
(8, 1, '2019-05-14'),
(24, 17, '2019-05-03'),
(319, 1, '2019-05-12'),
(24, 4, '2019-05-24'),
(24, 14, '2019-05-24'),
(48, 16, '2019-05-23'),
(54, 4, '2019-05-25'),
(55, 11, '2019-05-25'),
(318, 16, '2019-05-22'),
(2141, 4, '2019-05-13'),
(2506, 13, '2019-04-23'),
(2360, 14, '2019-04-23'),
(2654, 8, '2019-04-25'),
(2654, 3, '2019-04-26'),
(2705, 8, '2019-05-23'),
(2705, 16, '2019-05-23'),
(319, 18, '2019-05-23');

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `inventar`
--
ALTER TABLE `inventar`
  ADD CONSTRAINT `inventar_ibfk_1` FOREIGN KEY (`Nr_Patient`) REFERENCES `patienten` (`Nr`);

--
-- Constraints der Tabelle `patienten_indikationen`
--
ALTER TABLE `patienten_indikationen`
  ADD CONSTRAINT `patienten_indikationen_ibfk_1` FOREIGN KEY (`Nr_Patient`) REFERENCES `patienten` (`Nr`),
  ADD CONSTRAINT `patienten_indikationen_ibfk_2` FOREIGN KEY (`Nr_Indikation`) REFERENCES `indikationen` (`Nr`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
