-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 04. Mai 2016 um 21:18
-- Server-Version: 10.0.17-MariaDB
-- PHP-Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `softtech2016`
--
DROP SCHEMA IF EXISTS softtech2016;
CREATE SCHEMA softtech2016;
USE softtech2016;
SET AUTOCOMMIT=0;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `abteilungen`
--

CREATE TABLE `abteilungen` (
  `Abt_Nr` int(11) NOT NULL,
  `Bezeichnung` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `abteilungen`
--

INSERT INTO `abteilungen` (`Abt_Nr`, `Bezeichnung`) VALUES
(1, 'Geschaeftsleitung'),
(2, 'Verwaltung & Organisation'),
(3, 'Einkauf'),
(4, 'Produktion'),
(5, 'Verkauf');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gehaltsstufen`
--

CREATE TABLE `gehaltsstufen` (
  `GS_Nr` int(11) NOT NULL,
  `Bezeichnung` varchar(20) DEFAULT NULL,
  `Bruttoverdienst` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `gehaltsstufen`
--

INSERT INTO `gehaltsstufen` (`GS_Nr`, `Bezeichnung`, `Bruttoverdienst`) VALUES
(1, 'Vollzeit, Einsteiger', '1889.00'),
(2, 'Teilzeit, 50', '1734.00'),
(3, 'Vollzeit, Standard', '2476.00'),
(4, 'Vollzeit, Assistent', '3246.00'),
(5, 'Vollzeit, Leitung', '3837.00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mitarbeiter`
--

CREATE TABLE `mitarbeiter` (
  `P_Nr` int(11) NOT NULL,
  `V_Name` varchar(20) DEFAULT NULL,
  `N_Name` varchar(20) DEFAULT NULL,
  `Strasse` varchar(50) DEFAULT NULL,
  `HausNr` varchar(5) DEFAULT NULL,
  `PLZ` char(5) DEFAULT NULL,
  `Ort` varchar(20) DEFAULT NULL,
  `G_Datum` date DEFAULT NULL,
  `Geschlecht` char(1) DEFAULT NULL,
  `Abt_Nr` int(11) DEFAULT NULL,
  `GS_Nr` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `mitarbeiter`
--

INSERT INTO `mitarbeiter` (`P_Nr`, `V_Name`, `N_Name`, `Strasse`, `HausNr`, `PLZ`, `Ort`, `G_Datum`, `Geschlecht`, `Abt_Nr`, `GS_Nr`) VALUES
(1, 'Gerd', 'Waldmann', 'Parkstr.', '14', '91522', 'Ansbach', '1953-12-02', 'm', 1, 5),
(2, 'Herbert', 'Meier', 'Schwabacherstr.', '256', '91522', 'Ansbach', '1955-05-16', 'm', 2, 4),
(3, 'Susanne', 'Jacobi', 'Zeltnerstr.', '25', '90443', 'Nuernberg', '1958-03-09', 'w', 3, 2),
(4, 'Michaela', 'Petersen', 'Zollesstr.', '12', '91522', 'Ansbach', '1985-03-01', 'w', 5, 3),
(5, 'Akin', 'Matek', 'Laubenweg', '23', '91522', 'Ansbach', '1989-08-09', 'm', 3, 3),
(6, 'Roland', 'Meindl', 'Voltastr.', '8', '90459', 'Nuernberg', '1980-02-17', 'm', 4, 3),
(7, 'Martina', 'Unger', 'Eschenauer Str.', '19', '90411', 'Nuernberg', '1978-07-25', 'w', 5, 2),
(8, 'Beyza', 'Kaminski', 'Krugstr.', '63', '90419', 'Nuernberg', '1995-09-30', 'w', 4, 1),
(9, 'Sebastian', 'Brunna', 'Schuckertplatz', '3', '90459', 'Nuernberg', '1994-01-14', 'm', 2, 1),
(10, 'Sabine', 'Sobeck', 'Landgrabenstr.', '67', '90459', 'Nuernberg', '1971-10-08', 'w', 2, 3),
(11, 'Gerta', 'Braunstein', 'Amalienstr.', '35', '91522', 'Ansbach', '1960-03-24', 'w', 1, 5),
(12, 'Sabine', 'Meier', 'Parkstr. ', '9', '90419', 'Nuernberg', '1966-09-12', 'w', 2, 3),
(13, 'Peter', 'Hansen', 'Hugenottenplatz', '24', '91054', 'Erlangen', '1963-02-28', 'm', 5, 1),
(14, 'Gabriele', 'Kurz', 'Dieselstr.', '10', '91301', 'Forchheim', '1965-10-13', 'w', 4, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `schulungen`
--

CREATE TABLE `schulungen` (
  `S_Nr` int(11) NOT NULL,
  `Bezeichnung` varchar(100) NOT NULL,
  `Kursbeginn` date NOT NULL,
  `Kursende` date NOT NULL,
  `Kurskosten` decimal(10,0) NOT NULL,
  `V_Nr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `schulungen`
--

INSERT INTO `schulungen` (`S_Nr`, `Bezeichnung`, `Kursbeginn`, `Kursende`, `Kurskosten`, `V_Nr`) VALUES
(1, 'Word2010 Anfaenger', '2015-03-25', '2015-04-05', '424', 1),
(2, 'Word2010 Fortgeschrittene', '2015-04-07', '2015-04-18', '545', 1),
(3, 'SQL', '2015-04-08', '2015-04-24', '860', 4),
(4, 'C++ Programmierung', '2015-05-12', '2015-06-27', '1450', 2),
(5, 'Java Programmierung', '2015-10-06', '2015-12-12', '2300', 3),
(6, 'php Einfuehrung', '2015-04-28', '2015-05-16', '870', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `teilnehmer`
--

CREATE TABLE `teilnehmer` (
  `TN_Nr` int(11) NOT NULL,
  `P_Nr` int(11) NOT NULL,
  `S_Nr` int(11) NOT NULL,
  `Besucht` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `teilnehmer`
--

INSERT INTO `teilnehmer` (`TN_Nr`, `P_Nr`, `S_Nr`, `Besucht`) VALUES
(1, 2, 3, 0),
(2, 6, 5, 1),
(3, 7, 1, 1),
(4, 1, 2, 0),
(5, 3, 5, 0),
(6, 10, 4, 0),
(7, 5, 3, 0),
(8, 6, 1, 0),
(9, 9, 2, 1),
(10, 4, 6, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `veranstalter`
--

CREATE TABLE `veranstalter` (
  `V_Nr` int(11) NOT NULL,
  `Name` varchar(75) NOT NULL,
  `Strasse` varchar(100) NOT NULL,
  `HausNr` char(10) NOT NULL,
  `PLZ` char(5) NOT NULL,
  `Ort` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `veranstalter`
--

INSERT INTO `veranstalter` (`V_Nr`, `Name`, `Strasse`, `HausNr`, `PLZ`, `Ort`) VALUES
(1, 'Bildungszentrum Nuernberg', 'Am Platz ', '67', '90443', 'Nuernberg'),
(2, 'SoftwareAnwender', 'Muckerstr.', '55', '91522', 'Ansbach'),
(3, 'Microsoft Easy-Learn', 'Salzburgerstr.', '34', '86543', 'Muenchen'),
(4, 'IT Learning', 'Paulstr.', '9', '91357', 'Erlangen');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `abteilungen`
--
ALTER TABLE `abteilungen`
  ADD PRIMARY KEY (`Abt_Nr`);

--
-- Indizes für die Tabelle `gehaltsstufen`
--
ALTER TABLE `gehaltsstufen`
  ADD PRIMARY KEY (`GS_Nr`);

--
-- Indizes für die Tabelle `mitarbeiter`
--
ALTER TABLE `mitarbeiter`
  ADD PRIMARY KEY (`P_Nr`),
  ADD KEY `fk_ma_abt` (`Abt_Nr`),
  ADD KEY `fk_ma_gs` (`GS_Nr`);

--
-- Indizes für die Tabelle `schulungen`
--
ALTER TABLE `schulungen`
  ADD PRIMARY KEY (`S_Nr`),
  ADD KEY `fk_V_s` (`V_Nr`);

--
-- Indizes für die Tabelle `teilnehmer`
--
ALTER TABLE `teilnehmer`
  ADD PRIMARY KEY (`TN_Nr`),
  ADD KEY `fk_tn_s` (`S_Nr`),
  ADD KEY `fk_tn_m` (`P_Nr`);

--
-- Indizes für die Tabelle `veranstalter`
--
ALTER TABLE `veranstalter`
  ADD PRIMARY KEY (`V_Nr`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `mitarbeiter`
--
ALTER TABLE `mitarbeiter`
  ADD CONSTRAINT `fk_ma_abt` FOREIGN KEY (`Abt_Nr`) REFERENCES `abteilungen` (`Abt_Nr`),
  ADD CONSTRAINT `fk_ma_gs` FOREIGN KEY (`GS_Nr`) REFERENCES `gehaltsstufen` (`GS_Nr`);

--
-- Constraints der Tabelle `schulungen`
--
ALTER TABLE `schulungen`
  ADD CONSTRAINT `fk_V_s` FOREIGN KEY (`V_Nr`) REFERENCES `veranstalter` (`V_Nr`);

--
-- Constraints der Tabelle `teilnehmer`
--
ALTER TABLE `teilnehmer`
  ADD CONSTRAINT `fk_tn_m` FOREIGN KEY (`P_Nr`) REFERENCES `mitarbeiter` (`P_Nr`),
  ADD CONSTRAINT `fk_tn_s` FOREIGN KEY (`S_Nr`) REFERENCES `schulungen` (`S_Nr`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
