# Aufgabe 5.1
SELECT *
FROM tblchips, tblzutrittsversuche, tblbenutzer;
# Aufgabe 5.2
SELECT BenutzerID
FROM tblbenutzer INNER JOIN tblchips t on tblbenutzer.BenutzerID = t.tblBenutzer_BenutzerID
WHERE t.ChipSerienNr = '01104A3EE085';
# Aufgabe 5.3
SELECT BenutzerID, Nachname
FROM tblbenutzer INNER JOIN tblchips t on tblbenutzer.BenutzerID = t.tblBenutzer_BenutzerID
WHERE t.ChipSerienNr = '01104A3EE085';
# Aufgabe 5.4
SELECT ChipSerienNr
FROM tblbenutzer INNER JOIN tblchips t on tblbenutzer.BenutzerID = t.tblBenutzer_BenutzerID
WHERE tblbenutzer.Nachname = 'Nettmann';
# Aufgabe 5.5
SELECT COUNT(*) AS MAIER_DRIN
FROM tblzutrittsversuche AS vers
    INNER JOIN tblchips t on vers.tblChips_ChipsID = t.ChipsID
    INNER JOIN tblbenutzer t2 on t.tblBenutzer_BenutzerID = t2.BenutzerID
WHERE Nachname = 'Maier'
  AND Ergebnis = 'Zutritt gestattet';
# Aufgabe 5.6
SELECT Nachname, COUNT (*) AS ABGELEHNT
FROM tblzutrittsversuche AS vers
    INNER JOIN tblchips t on vers.tblChips_ChipsID = t.ChipsID
    INNER JOIN tblbenutzer t2 on t.tblBenutzer_BenutzerID = t2.BenutzerID
WHERE Ergebnis = 'Zutritt abgelehnt'
GROUP BY Nachname
ORDER BY ABGELEHNT DESC;
# Aufgabe 5.7
SELECT Nachname, COUNT (*) AS ABGELEHNT
FROM tblzutrittsversuche AS vers
    INNER JOIN tblchips t on vers.tblChips_ChipsID = t.ChipsID
    INNER JOIN tblbenutzer t2 on t.tblBenutzer_BenutzerID = t2.BenutzerID
WHERE Ergebnis = 'Zutritt abgelehnt'
GROUP BY Nachname
HAVING ABGELEHNT > 8
ORDER BY ABGELEHNT DESC;
# Aufgabe 5.8
SELECT Nachname, COUNT (Ergebnis) AS ZUTRITTE
FROM tblzutrittsversuche AS vers
    RIGHT JOIN tblchips t on vers.tblChips_ChipsID = t.ChipsID
    RIGHT JOIN tblbenutzer t2 on t.tblBenutzer_BenutzerID = t2.BenutzerID
GROUP BY Nachname
ORDER BY ZUTRITTE, Nachname;
# Aufgabe 5.9
SELECT Nachname, MAX(Zeitstempel) AS LETZTER_VERSUCH
FROM tblzutrittsversuche AS vers
    INNER JOIN tblchips t on vers.tblChips_ChipsID = t.ChipsID
    INNER JOIN tblbenutzer t2 on t.tblBenutzer_BenutzerID = t2.BenutzerID
WHERE Ergebnis = 'Zutritt abgelehnt'
GROUP BY Nachname
ORDER BY LETZTER_VERSUCH;
# Aufgabe 5.10
SELECT Nachname, MAX(Zeitstempel) AS LETZTER_VERSUCH, MIN(Zeitstempel) AS ERSTER_VERSUCH
FROM tblzutrittsversuche AS vers
    INNER JOIN tblchips t on vers.tblChips_ChipsID = t.ChipsID
    INNER JOIN tblbenutzer t2 on t.tblBenutzer_BenutzerID = t2.BenutzerID
WHERE Ergebnis = 'Zutritt abgelehnt'
GROUP BY Nachname
ORDER BY Nachname;
# Aufgabe 5.11
SELECT Nachname, COUNT (*) AS ABGELEHNT
FROM tblzutrittsversuche AS vers
    INNER JOIN tblchips t on vers.tblChips_ChipsID = t.ChipsID
    INNER JOIN tblbenutzer t2 on t.tblBenutzer_BenutzerID = t2.BenutzerID
WHERE Ergebnis = 'Zutritt abgelehnt'
GROUP BY Nachname
HAVING ABGELEHNT > (
        (SELECT COUNT(*) FROM tblzutrittsversuche WHERE Ergebnis = 'Zutritt abgelehnt')/
        (SELECT COUNT( DISTINCT Nachname) FROM tblzutrittsversuche AS vers
            INNER JOIN tblchips t on vers.tblChips_ChipsID = t.ChipsID
            INNER JOIN tblbenutzer t2 on t.tblBenutzer_BenutzerID = t2.BenutzerID)
    )
ORDER BY ABGELEHNT DESC;
