# Aufgabe 2

# Aufgabe 2.1
SELECT *
FROM tblzutrittsversuche;
# Aufgabe 2.2
SELECT *
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt abgelehnt';
# Aufgabe 2.3
SELECT *
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt gestattet'
ORDER BY tblChips_ChipsID;
# Aufgabe 2.4
SELECT Zeitstempel
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt abgelehnt'
ORDER BY Zeitstempel DESC;
# Aufgabe 2.5
SELECT DISTINCT tblChips_ChipsID
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt abgelehnt'
ORDER BY tblChips_ChipsID;
# Aufgabe 2.6
SELECT DISTINCT tblChips_ChipsID
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt gestattet'
ORDER BY tblChips_ChipsID;
# Aufgabe 2.7
SELECT COUNT(*) AS Gestattet
FROM tblzutrittsversuche
WHERE Zeitstempel LIKE '2017-11-22%'
  AND Ergebnis = 'Zutritt gestattet';
# Aufgabe 2.8
SELECT *
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt abgelehnt'
  AND tblChips_ChipsID LIKE '1%';
# Aufgabe 2.9
SELECT tblChips_ChipsID, COUNT(*)
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt gestattet'
GROUP BY tblChips_ChipsID;
# Aufgabe 2.10
SELECT tblChips_ChipsID, COUNT(*) AS Zutritte
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt gestattet'
GROUP BY tblChips_ChipsID
HAVING Zutritte > 10;
# Aufgabe 2.11
SELECT SUM(tblChips_ChipsID)
FROM tblzutrittsversuche;
# Aufgabe 2.12
SELECT tblChips_ChipsID
FROM tblzutrittsversuche
WHERE tblChips_ChipsID NOT IN (SELECT tblChips_ChipsID AS Anzahl
    FROM tblzutrittsversuche
    WHERE Ergebnis = 'Zutritt gestattet')
GROUP BY tblChips_ChipsID;
# Aufgabe 2.13
SELECT tblChips_ChipsID, MIN(Zeitstempel) AS Datum
FROM tblzutrittsversuche
GROUP BY tblChips_ChipsID
ORDER BY tblChips_ChipsID;

SELECT MIN(Zeitstempel) AS Datum
FROM tblzutrittsversuche
GROUP BY tblChips_ChipsID
ORDER BY tblChips_ChipsID;

SELECT tblChips_ChipsID, Ergebnis
FROM tblzutrittsversuche
WHERE Zeitstempel IN (
    SELECT MIN(Zeitstempel) AS Datum
    FROM tblzutrittsversuche
    GROUP BY tblChips_ChipsID)
ORDER BY tblChips_ChipsID;

SELECT tblChips_ChipsID, Ergebnis, Zeitstempel
FROM tblzutrittsversuche AS AeuSelect
WHERE Zeitstempel = (
    SELECT MIN(Zeitstempel)
    FROM tblzutrittsversuche AS InnSelect
    WHERE InnSelect.tblChips_ChipsID = AeuSelect.tblChips_ChipsID)
ORDER BY tblChips_ChipsID;
# Aufgabe 2.14
SELECT MIN(Zeitstempel) AS Erster_Zutritt
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt gestattet';
# Aufgabe 2.15
SELECT tblChips_ChipsID, COUNT(*) AS SUMME_ABELEHNT
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt abgelehnt'
GROUP BY tblChips_ChipsID
ORDER BY SUMME_ABELEHNT DESC
LIMIT 1;
# Aufgabe 2.16
SELECT tblChips_ChipsID, MAX(Zeitstempel) AS LETZTER_ZUTRITT
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt gestattet'
GROUP BY tblChips_ChipsID
ORDER BY tblChips_ChipsID;
# Aufgabe 2.17
SELECT tblChips_ChipsID, MAX(Zeitstempel) AS LETZTER_ZUTRITT, Ergebnis
FROM tblzutrittsversuche
GROUP BY tblChips_ChipsID, Ergebnis
ORDER BY tblChips_ChipsID;
# Aufgabe 2.18
SELECT tblChips_ChipsID, COUNT(Ergebnis) AS ZUTRITTS_SUMME, Ergebnis
FROM tblzutrittsversuche
GROUP BY tblChips_ChipsID, Ergebnis
ORDER BY tblChips_ChipsID;
# Aufgabe 2.19
SELECT tblChips_ChipsID, COUNT(Ergebnis) AS ZUTRITTS_SUMME_ABGELEHNT, Ergebnis
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt abgelehnt'
GROUP BY tblChips_ChipsID, Ergebnis
ORDER BY tblChips_ChipsID;

SELECT tblChips_ChipsID, COUNT(Ergebnis) AS ZUTRITTS_SUMME_ERLAUBT, Ergebnis
FROM tblzutrittsversuche
WHERE Ergebnis = 'Zutritt gestattet'
GROUP BY tblChips_ChipsID, Ergebnis
ORDER BY tblChips_ChipsID;

# Falschrum
SELECT tblChips_ChipsID
FROM (SELECT tblChips_ChipsID, COUNT(Ergebnis) AS ZUTRITTS_SUMME_ERLAUBT, Ergebnis
    FROM tblzutrittsversuche
    WHERE Ergebnis = 'Zutritt gestattet'
    GROUP BY tblChips_ChipsID, Ergebnis
    ORDER BY tblChips_ChipsID) AS TEMP
WHERE ZUTRITTS_SUMME_ERLAUBT < (SELECT COUNT(Ergebnis) AS ZUTRITTS_SUMME_ABGELEHNT
        FROM tblzutrittsversuche
        WHERE Ergebnis = 'Zutritt abgelehnt' AND TEMP.tblChips_ChipsID = TEMP.tblChips_ChipsID
        ORDER BY tblChips_ChipsID);

# Keine Ergebnisse
SELECT tblChips_ChipsID
FROM (SELECT tblChips_ChipsID, COUNT(Ergebnis) AS ZUTRITTS_SUMME_ERLAUBT, Ergebnis
    FROM tblzutrittsversuche
    WHERE Ergebnis = 'Zutritt abgelehnt'
    GROUP BY tblChips_ChipsID, Ergebnis
    ORDER BY tblChips_ChipsID) AS TEMP
WHERE ZUTRITTS_SUMME_ERLAUBT > (SELECT COUNT(Ergebnis) AS ZUTRITTS_SUMME_ABGELEHNT
        FROM tblzutrittsversuche
        WHERE Ergebnis = 'Zutritt gestattet' AND TEMP.tblChips_ChipsID = TEMP.tblChips_ChipsID
        ORDER BY tblChips_ChipsID);