# Aufgabe 6.1
CREATE TABLE tblreader (
	ReaderID int NOT NULL AUTO_INCREMENT,
	Bezeichnung varchar(30),
  		PRIMARY KEY(ReaderID)
);


# Aufgabe 6.2 + 6.3
CREATE TABLE tblBerechtigung(
	tblchips_ChipsID int,
	tblreader_ReaderID int,
		PRIMARY KEY(tblchips_ChipsID, tblreader_ReaderID),
  			CONSTRAINT foreign_key_chipid FOREIGN KEY (tblchips_ChipsID) REFERENCES tblchips(ChipsID),
			CONSTRAINT foreign_key_readerid FOREIGN KEY (tblreader_ReaderID) REFERENCES tblreader(ReaderID)
);


# Aufgabe 6.4
# source "\\...";

# Aufgabe 6.5
INSERT INTO tblbenutzer (Nachname) VALUES ('Biedermann');
SELECT * FROM tblbenutzer WHERE Nachname = 'Biedermann';
# Aufgabe 6.6
INSERT INTO tblchips (ChipSerienNr, tblBenutzer_BenutzerID) VALUES ('0123456789ABCD', 8);
# Aufgabe 6.7
INSERT INTO tblreader (Bezeichnung) VALUES ('Toilette');
# Aufgabe 6.8
INSERT INTO tblberechtigung (tblChips_ChipsID, tblReader_ReaderID)
VALUES ((SELECT ChipsID
            FROM tblchips INNER JOIN tblbenutzer t on tblchips.tblBenutzer_BenutzerID = t.BenutzerID
            WHERE t.Nachname = 'Nettmann'),
        (SELECT ReaderID FROM tblreader WHERE Bezeichnung = 'Toilette'));
# Aufgabe 6.9
INSERT IGNORE INTO tblberechtigung (tblChips_ChipsID, tblReader_ReaderID)
(SELECT ChipsID, ReaderID
FROM tblchips INNER JOIN tblbenutzer t on tblchips.tblBenutzer_BenutzerID = t.BenutzerID,
     tblreader
WHERE t.Nachname = 'Nettmann');

# Aufgabe 6.10
DELETE FROM tblBerechtigung
WHERE tblChips_ChipsID = (
    SELECT t.ChipsID
    FROM tblbenutzer
        INNER JOIN tblchips t on tblbenutzer.BenutzerID = t.tblBenutzer_BenutzerID
    WHERE Nachname = 'Tabor');

UPDATE tblchips
INNER JOIN tblbenutzer on tblbenutzer.BenutzerID = tblBenutzer_BenutzerID
SET tblBenutzer_BenutzerID = NULL
WHERE ChipsID = (
    SELECT ChipsID
    WHERE Nachname = 'Tabor');

DELETE FROM tblbenutzer
WHERE (
    SELECT BenutzerID
    WHERE Nachname = 'Tabor');

# Aufgabe 6.11
UPDATE tblbenutzer
SET Nachname = 'Schmidt-Huber'
WHERE BenutzerID = (
    SELECT BenutzerID
    WHERE Nachname='Schmidt');

SELECT *
FROM tblbenutzer
WHERE Nachname = 'Schmidt-Huber';

# Aufgabe 6.12
DELETE FROM tblBerechtigung
WHERE tblReader_ReaderID = (
    SELECT ReaderID
    FROM tblreader
    WHERE Bezeichnung = 'Eingang Lager' AND NOT tblChips_ChipsID = (
        SELECT ChipsID
        FROM tblchips INNER JOIN tblbenutzer t on tblchips.tblBenutzer_BenutzerID = t.BenutzerID
        WHERE Nachname = 'Nettmann'
        )
    );
