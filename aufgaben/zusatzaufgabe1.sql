# Vorbereitung
CREATE DATABASE IF NOT EXISTS lieferantendb;
USE lieferantendb;

# Aufgabe 1.1
CREATE TABLE IF NOT EXISTS tblLiferanten (
    liferantennummer bigint PRIMARY KEY NOT NULL,
    name varchar(30),
    ort varchar(30),
    plz varchar(7),
    ansprechpartner varchar(30),
    lieferdauerinwochen int
);

# Aufgabe 1.2
INSERT INTO tblLiferanten (liferantennummer, name, ort, plz, ansprechpartner, lieferdauerinwochen)
VALUES (125, 'Berghaus', 'Remscheid', '52336', 'Herr Günther', 4),
       (166, 'Däubler', 'Fürth', '90765', 'Frau Lot', 5),
       (321, 'Rietle', 'Nürnberg', '90405', 'Herr Kern', 4);

# Aufgabe 1.3
UPDATE tblLiferanten
SET name = 'Riedel', ansprechpartner = 'Frau Kraft'
WHERE liferantennummer = 321;

UPDATE tblLiferanten
SET ort = 'Lennep', plz = '52664'
WHERE liferantennummer = 125;

# Aufgabe 1.4
DELETE FROM tblLiferanten
WHERE liferantennummer = 166;

# Aufgabe 1.5
UPDATE tblLiferanten
SET lieferdauerinwochen = 2
WHERE lieferdauerinwochen = 4;

# Aufgabe 1.6
INSERT INTO tblLiferanten (liferantennummer, name, ort)
VALUES (554, 'König', 'Darmstadt');

# Aufgabe 1.7
ALTER TABLE tblLiferanten
ADD COLUMN zahlungsziel tinytext;

UPDATE tblLiferanten
SET zahlungsziel = 'Zwei Wochen nach Erhalt der Ware'
WHERE lieferdauerinwochen < 4;

# Falls es keine Zahlungseinschränkungen gibt, ist das Feld leer.
