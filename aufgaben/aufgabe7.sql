USE olympicgames;

# Aufgabe 7.1
INSERT INTO athlete (forename, surname, height, weight, countryCode, genderCode, sportID)
VALUES ('Johann', 'Müller', 188, 100, 'GER', 'M', 18);
# Aufgabe 7.2
UPDATE athlete
SET surname='Lenz'
WHERE surname='Bohme' AND forename='Marcus';
# Aufgabe 7.3
DELETE FROM athlete
WHERE surname='Lovassy' AND forename='Krisztian' AND genderCode='M';
# Aufgabe 7.4
SELECT *
FROM country
WHERE country LIKE '%team%';

UPDATE country
SET country='Australia'
WHERE countryCode='AUS';
UPDATE country
SET country='Great Britain'
WHERE countryCode='GBR';
UPDATE country
SET country='United States of America'
WHERE countryCode='USA';
# Aufgabe 7.5
SELECT *
FROM sport
WHERE category=subcategory AND specification IS NULL;

UPDATE sport
SET subcategory=NULL
WHERE sportID = (SELECT sportID
    WHERE category=subcategory AND specification IS NULL
);
# Aufgabe 7.6
INSERT INTO country (Country,CountryCode)
VALUES  ('Guinea-Bissau','GBS'),
('Iran','IRI'),
('Georgia','GEO'),
('Kyrgyzstan','KGZ'),
('Latvia','LAT'),
('Tajikistan','TJK'),
('Iraq','IRQ');

INSERT INTO sport (SportID,Category,Subcategory,Specification)
VALUES (53,'Wrestling','Freestyle','66kg'),
(54,'Wrestling','Freestyle','74kg'),
(55,'Wrestling','Freestyle','120kg'),
(56,'Wrestling','Greco-Roman','66kg'),
(57,'Wrestling','Freestyle','55kg'),
(58,'Wrestling','Freestyle','96kg'),
(59,'Wrestling','Greco-Roman','55kg'),
(60,'Wrestling','Greco-Roman','120kg'),
(61,'Wrestling','Freestyle','84kg'),
(62,'Wrestling','Greco-Roman','74kg'),
(63,'Wrestling','Greco-Roman','84kg'),
(64,'Wrestling','Freestyle','63kg');

INSERT INTO olympicgames.athlete (Forename,Surname,Height,Weight,CountryCode,GenderCode,SportID)
VALUES ('Andriy','Kvyatkovskyy',173,71,'UKR','M',53),
('Augusto','Midana',167,74,'GBS','M',54),
('Rares','Daniel Chintoan',189,120,'ROU','M',55),
('Steeve','Guenot',172,66,'FRA','M',56),
('Radoslav','Marinov Velikov',162,55,'BUL','M',57),
('Abdusalam','Gadisov',178,84,'RUS','M',58),
('Khetag','Pliev',182,96,'CAN','M',58),
('Sadegh','Saeed Goudarzi',179,74,'IRI','M',54),
('Chuluunbat','Jargalsaikhan',184,116,'MGL','M',55),
('Ayhan','Karakus',165,55,'TUR','M',59),
('Abdelrahman','Eltrabily',190,120,'EGY','M',60),
('Dato','Marsagishvili',186,84,'GEO','M',61),
('Daniyar','Kobonov',180,74,'KGZ','M',62),
('Vladimer','Khinchegashvili',170,55,'GEO','M',57),
('Nenad','Zugaj',177,84,'CRO','M',63),
('Tsutomu','Fujimura',174,66,'JPN','M',56),
('Christophe','Guenot',178,74,'FRA','M',62),
('Khasan','Baroev',188,120,'RUS','M',60),
('Haakan','Erik Nyblom',160,55,'DEN','M',59),
('Bilel','Ouechtati',172,74,'TUN','M',54),
('Anastasija','Grigorjeva',169,62,'LAT','W',64),
('Justin','Dashaun Lester',170,73,'USA','M',56),
('Marcia','Yuleisi Andrades',165,55,'VEN','W',57),
('Rustam','Iskandari',176,96,'TJK','M',58),
('Abdou','Omar Abdou Ahmed',172,66,'EGY','M',53),
('Anzor','Urishev',177,84,'RUS','M',61),
('Wuileixis','de Jesus Rivas',165,60,'VEN','M',56),
('Ali','Nadhim Salman',190,120,'IRQ','M',60),
('Olga','Butkevych',160,60,'GBR','W',57);
# Aufgabe 7.7
INSERT INTO gender (genderCode, gender) VALUES ('D', 'divers');
UPDATE athlete
SET genderCode = 'D'
WHERE athleteID IN (2,18,93,501);
# Aufgabe 7.8
DELETE FROM athlete
WHERE countryCode = 'RUS';

DELETE FROM country
WHERE countryCode = 'RUS';
# Aufgabe 7.9
# ID 52: Volleyball

SELECT *
FROM athlete
WHERE sportID = 52;

DELETE FROM athlete
WHERE sportID = 52;

DELETE FROM sport
WHERE sportID = 52;
