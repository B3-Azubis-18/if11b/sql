# 3.1
SELECT *
FROM tblZutrittsversuche, tblChips;
# 3.2
SELECT ChipSerienNr
FROM tblchips
WHERE ChipSerienNr LIKE '011%';
# 3.3
SELECT tblchips.ChipSerienNr, tblzutrittsversuche.ZutrittsversuchID
FROM tblzutrittsversuche, tblchips
WHERE tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID;
# 3.4
SELECT tblchips.ChipSerienNr, tblzutrittsversuche.ZutrittsversuchID
FROM tblzutrittsversuche INNER JOIN tblchips ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID;
# 3.5
SELECT tblBenutzer_BenutzerID
FROM tblzutrittsversuche INNER JOIN tblchips ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID
WHERE tblzutrittsversuche.ZutrittsversuchID = 2300;
# 3.6
SELECT tblzutrittsversuche.Zeitstempel
FROM tblzutrittsversuche INNER JOIN tblchips ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID
WHERE tblchips.tblBenutzer_BenutzerID = 5 AND tblzutrittsversuche.Ergebnis = 'Zutritt abgelehnt'
ORDER BY tblzutrittsversuche.Zeitstempel DESC;
# 3.7
SELECT ZutrittsversuchID, ChipSerienNr
FROM tblzutrittsversuche INNER JOIN tblchips ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID
WHERE Zeitstempel LIKE '2017-11-20%' AND Ergebnis = 'Zutritt gestattet';
# 3.8
SELECT tblBenutzer_BenutzerID, COUNT(*)
FROM tblzutrittsversuche INNER JOIN tblchips ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID
WHERE Ergebnis = 'Zutritt abgelehnt'
GROUP BY tblBenutzer_BenutzerID;
# 3.9
SELECT tblBenutzer_BenutzerID, COUNT (Ergebnis) AS Zutrittsversuche
FROM tblchips LEFT JOIN tblzutrittsversuche ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID
GROUP BY tblBenutzer_BenutzerID;
# 3.10
SELECT tblBenutzer_BenutzerID, COUNT (Ergebnis) AS Zutrittsversuche
FROM tblzutrittsversuche RIGHT JOIN tblchips ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID
GROUP BY tblBenutzer_BenutzerID;
# 3.11
SELECT tblBenutzer_BenutzerID, COUNT(Ergebnis) AS Zutrittsversuche
FROM tblchips LEFT JOIN tblzutrittsversuche ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID
WHERE Ergebnis != 'Zutritt gestattet' OR Ergebnis IS NULL
GROUP BY tblBenutzer_BenutzerID;
# oder
SELECT tblBenutzer_BenutzerID, COUNT(Ergebnis) AS Zutrittsversuche, Ergebnis
FROM tblchips LEFT JOIN tblzutrittsversuche ON tblzutrittsversuche.tblChips_ChipsID = tblchips.ChipsID
WHERE Ergebnis != 'Zutritt gestattet' OR Ergebnis IS NULL
GROUP BY tblBenutzer_BenutzerID, Ergebnis;
